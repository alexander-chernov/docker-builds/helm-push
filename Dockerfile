FROM alpine:3.11 as build
ARG BASE_URL=https://get.helm.sh
ARG HELM_VERSION=3.2.0

RUN apk add --update --no-cache curl ca-certificates git \
    && curl -L ${BASE_URL}/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar xvz \
    && mv linux-amd64/helm /usr/bin/helm \
    && chmod +x /usr/bin/helm \
    && rm -rf linux-amd64 \
    && apk del curl \
    && rm -f /var/cache/apk/*

RUN adduser --disabled-password  helm-runner \
    && su -c "helm plugin install https://github.com/chartmuseum/helm-push" helm-runner \
    && su -c "helm repo add stable https://kubernetes-charts.storage.googleapis.com" helm-runner

WORKDIR /tmp

USER helm-runner

CMD ["helm"]
